import { Progress } from 'antd';
import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { movieServ } from '../../service/movieService';

export default function DetailPage() {
    let { id } = useParams();
    const [movie, setMovie] = useState({});
    useEffect(() => {
        let fetchDetail = async () => {
            try {
                let result = await movieServ.getDetailMovie(id);
                setMovie(result.data.content)
                console.log("🚀 ~ file: DetailPage.js:11 ~ fetchDetail ~ result:", result)
            } catch (error) {
                console.log("🚀 ~ file: DetailPage.js:12 ~ fetchDetail ~ error:", error)

            }

        }
        fetchDetail();
    }, []);
    // Async, Await ~ then catch
    return (
        <div className="container">
            <div className="flex">
                <img src={movie.hinhAnh} className="w-1/3" alt="" srcset="" />

                <div className="space-y-5 px-5">
                    <h2 className='font-medium'>{movie.tenPhim}</h2>
                    <h2>{movie.moTa}</h2>
                    <Progress percent={movie.danhGia * 10} />
                    <div>
                        <NavLink to={`/booking/${id}`} className="rounded px-5 py-2 bg-red-600 text-white font-light">
                            Mua vé
                        </NavLink>

                    </div>
                </div>
            </div>


        </div>
    )
}
