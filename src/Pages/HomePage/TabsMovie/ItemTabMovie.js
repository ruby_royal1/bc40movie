import React from 'react'
import moment from 'moment'
export default function ItemTabMovie({ phim }) {
    console.log("🚀 ~ file: ItemTabMovie.js:4 ~ ItemTabMovie ~ phim:", phim)
    return (
        <div className='p-5 flex space-x-10'>
            <img src={phim.hinhAnh} alt="" className='w-24 h-32' />
            <div>
                <h3>{phim.tenPhim}</h3>
                <div className='grid grid-cols-3 gap-5'>
                    {phim.lstLichChieuTheoPhim.slice(0, 9).map((item) => {
                        return (
                            <span className="rounded p-2 bg-red-500 text-white font-medium">
                                {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY ~ hh:mm")}
                            </span>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}
