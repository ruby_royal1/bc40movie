import axios from "axios";
import { BASE_URL, configHeaders, http } from "./config";

export const movieServ = {
  getMovieList: () => {
    return http.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01")
    // cách 1: gọi API qua Axios instance
  },
  getMovieByTheater: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap`,
      method: "GET",
      headers: configHeaders(),
      // Cách 2: gọi thông thường
    });
  },
  getDetailMovie: (maPhim) => {
    return http.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`)
  }
};

